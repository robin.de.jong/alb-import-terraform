package main

import (
	"alb-import/pkg/aws"
	"context"
	"log"
)

func main() {
	ctx := context.Background()
	elb := aws.NewELB(ctx)
	lbs := elb.FindAll(ctx)
	for _, lb := range lbs {
		log.Println(lb.Name, lb.ARN)
	}
}
