package aws

import (
	"context"
	"log"

	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/elasticloadbalancingv2"
)

type ELB struct {
	client elasticloadbalancingv2.Client
}

type LoadBalancer struct {
	ARN  string
	Name string
}

func NewELB(ctx context.Context) *ELB {
	elb := ELB{}
	elb.setAWSClient("us-east-1", ctx)

	return &elb
}

func (e *ELB) FindAll(ctx context.Context) []LoadBalancer {
	input := elasticloadbalancingv2.DescribeLoadBalancersInput{}

	results, err := e.client.DescribeLoadBalancers(ctx, &input)
	if err != nil {
		log.Fatal("Error fetching listeners:", err)
	}

	arr := []LoadBalancer{}
	for _, result := range results.LoadBalancers {
		arr = append(arr, LoadBalancer{ARN: *result.LoadBalancerArn, Name: *result.LoadBalancerName})
	}
	return arr
}

func (e *ELB) setAWSClient(region string, ctx context.Context) {
	cfg, err := config.LoadDefaultConfig(ctx, config.WithRegion(region))

	if err != nil {
		log.Fatal("Error setting config:", err)
	}

	e.client = *elasticloadbalancingv2.NewFromConfig(cfg)
}
