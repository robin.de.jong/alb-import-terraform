package template

import (
	"bytes"
	"io/ioutil"
	"log"
	"strings"
	"text/template"
)

type PolicyTemplate struct {
	Name           string
	ARN            string
	Path           string
	Description    string
	PolicyName     string
	PolicyLocation string
}

const PolicyFileLocation = "files/template/aws_iam_policy.tf"
const GroupFileLocation = "files/template/group.tf"
const UserFileLocation = "files/template/user.tf"

var Funcs = template.FuncMap{
	"join":     strings.Join,
	"join_key": Joinkey,
}

func ConvertTemplate(data map[string]any, file string) string {
	content, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatalln("Error opening:", PolicyFileLocation, "with error:", err)
		return ""
	}
	// template, err := template.New("Hello").ParseFiles(PolicyFileLocation)
	template, err := template.New("Hello").Funcs(Funcs).Parse(string(content))
	if err != nil {
		log.Fatalln("Error parsing template, with error:", err)
		return ""
	}

	var buffer bytes.Buffer

	err = template.Execute(&buffer, data)
	return buffer.String()
}

func Joinkey(elems []map[string]string, key string, sep string) string {
	newElems := []string{}

	for _, elem := range elems {
		for elemKey, value := range elem {
			if elemKey == key {
				newElems = append(newElems, value)
			}
		}
	}
	return strings.Join(newElems, sep)
}
